<?php

/**
 * @file
 * Install/update/uninstall functions for the Find It installation profile.
 */

use Drupal\user\Entity\User;
use Drupal\shortcut\Entity\Shortcut;

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 *
 * @see system_install()
 */
function findit_install() {
  // Assign user 1 the "administrator" role.
  $user = User::load(1);
  $user->roles[] = 'administrator';
  $user->save();

  // We install some menu links, so we have to rebuild the router, to ensure the
  // menu links are valid.
  \Drupal::service('router.builder')->rebuildIfNeeded();

  // Populate the default shortcut set.
  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('Add content'),
    'weight' => -20,
    'link' => ['uri' => 'internal:/node/add'],
  ]);
  $shortcut->save();

  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('All content'),
    'weight' => -19,
    'link' => ['uri' => 'internal:/admin/content'],
  ]);
  $shortcut->save();
}


/**
 * Migrate all the Individual contacts into organization Contacts
 */
function findit_update_8001(&$sandbox) {
  if (!isset($sandbox['total'])) {
    $user_ids = \Drupal::entityQuery('user')->execute();
    $sandbox['total'] = count($user_ids);
    $sandbox['current'] = 0;
  }
  $users_per_batch = 50;

  $user_ids = \Drupal::entityQuery('user')
    ->range($sandbox['current'], $sandbox['current'] + $users_per_batch)
    ->execute();

  foreach($user_ids as $id) {
    findit_merge_profiles($id);
    $sandbox['current']++;
  }
  \Drupal::messenger()->addMessage($sandbox['current'] . ' users processed.');
  if ($sandbox['total'] == 0) {
    $sandbox['#finished'] = 1;
  } else {
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }
}

/**
 * Add the contact role (crm_org) to all the users.
 */
function findit_update_8002(&$sandbox) {
  if (!isset($sandbox['total'])) {
    $user_ids = \Drupal::entityQuery('user')->execute();
    $sandbox['total'] = count($user_ids);
    $sandbox['current'] = 0;
  }
  $users_per_batch = 50;
  $user_ids = \Drupal::entityQuery('user')
    ->range($sandbox['current'], $users_per_batch)
    ->execute();

  foreach($user_ids as $id) {
    /** @var $user \Drupal\user\UserInterface */
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($id);
    if (!$user->hasRole('crm_org')) {
      $user->addRole('crm_org');
      $user->save();
    }
    $sandbox['current']++;
  }
  \Drupal::messenger()->addMessage($sandbox['current'] . ' users processed.');
  if ($sandbox['total'] == 0) {
    $sandbox['#finished'] = 1;
  } else {
    $sandbox['#finished'] = ($sandbox['current'] / $sandbox['total']);
  }
}
